const express = require('express');
const http = require('http');
const bodyParser = require('body-parser');
const handleError = require('atb-exp-api-handle-error');
const listEndpoints = require('express-list-endpoints');
const { validate } = require('atb-exp-api-validate');

const app = express();
app.use(bodyParser.json());
const expressSwaggerGenerator = require('express-swagger-generator')(app);

const { createLogger } = require('./utils/logger');
const { routes } = require('./routes');

const server = http.createServer(app);

const log = createLogger({});

const PORT = 3000;

const options = {
  swaggerDefinition: {
    info: {
      description: 'Services of sending emails',
      title: 'Swagger',
      version: '1.0.0',
    },
    host: 'localhost:7890',
    basePath: '/',
    produces: [
      'application/json',
    ],
    schemes: ['http'],
  },
  basedir: __dirname, // app absolute path
  files: ['./routes/*.js'], // Path to the API handle folder
};
const schema = expressSwaggerGenerator(options);

require('atb-exp-api-validate').init(schema, log);
require('atb-exp-api-handle-error').init(log);

// inject log using middleware
app.use((req, _res, next) => {
  req.log = log;
  next();
});

app.use(bodyParser.json());
app.use(validate);

routes(app);

server.listen(PORT, () => {
  log.info(`Listening on port ${PORT}...`);
});

const registeredEndpoints = listEndpoints(app);
require('atb-exp-api-validate').setEndpoints(registeredEndpoints);

app.use(handleError);

app.all('*', (req, res, next) => {
  handleError(new Error(JSON.stringify({
    errorNum: 'ATB-exp-widgets-404',
    message: `Bad endpoint/method requested. Path: ${req.path}`,
    statusCode: 404,
  })), req, res, next);
});

process.on('uncaughtException', (err) => {
  log.error('Uncaught exception:', err.message);
  if (app.shutDownGracefully) {
    app.shutDownGracefully(err, () => {
      process.exit(1);
    });
  }
});

app.shutDownGracefully = (err, cb) => {
  if (server && server.close) {
    server.close(() => {
      log.error('Gracefully shutting down on uncaught exception:', err);
      if (cb) {
        cb('Graceful shutdown complete.');
      }
    });
  } else {
    log.error('Shutting down on uncaught exception:', err);
    if (cb) {
      cb('Shutdown complete.');
    }
  }
};

app.close = () => {
  log.info(`SHUTTING DOWN Express HTTP Node ${process.version} server listening on port ${PORT}`);
  server.close();
};


module.exports = app;
