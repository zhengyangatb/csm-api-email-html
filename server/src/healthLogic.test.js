const chai = require('chai');
const { healthLogic } = require('./healthLogic');

const { expect } = chai;

describe('health check logic ', () => {
  const versionData = '1.0.0';
  const buildData = '100';

  it('returns build and version in response', () => {
    const expectedResponse = {
      status: 200,
      data: {
        service: 'csm-api-email',
        healthy: true,
        version: 'v1.0.0 build 100',
      },
    };

    const response = healthLogic(versionData, buildData);
    expect(response).to.deep.equal(expectedResponse);
  });
});
