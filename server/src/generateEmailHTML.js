const Email = require('email-templates');

const generateEmailHTML = async (theme, parameters) => {
  const email = new Email();
  const html = await email.render(`../template/${theme}/html`, parameters);
  return html;
};

module.exports = { generateEmailHTML };
