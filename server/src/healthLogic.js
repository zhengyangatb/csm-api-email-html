const { logger } = require('../utils/logger');
const { STATUS_OK } = require('../config');
/**
 * Health - returns data for health endpoint
 * @version {string} version number
 * @build {string} build number
 */
const healthLogic = (version, build) => {
  logger({ type: 'debug', message: '/health check requested' });

  return {
    status: STATUS_OK,
    data: {
      service: 'csm-api-email-template',
      healthy: true,
      version: `v${version} build ${build}`,
    },
  };
};

module.exports = { healthLogic };
