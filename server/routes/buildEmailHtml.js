const { generateEmailHTML } = require('../src/generateEmailHTML');
/**
 * @typedef emailData
 * @property {string} theme.required - email theme
 * @property {object} parameters.required - email parameters
 */

/**
 * build email HTML route
 * @route POST /buildEmailHtml
 * @group alerts-notifications
 * @param {emailData.model} data.body.required - request body required
 * @returns {string} 201 - sent successful
 * @returns {Error} default - Unexpected error
 */
const buildEmailHtml = async (req, res) => {
  const { theme, parameters } = req.body;
  const html = await generateEmailHTML(theme, parameters);
  res.status(201).json(html);
};

module.exports = { buildEmailHtml };
