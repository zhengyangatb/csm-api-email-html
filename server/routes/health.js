const { version } = require('../../package');
const { build } = require('../../build');
const { healthLogic } = require('../src/healthLogic');

/**
 * Health check for email
 * @route GET /health
 * @group alerts-notifications
 * @returns {string} 200 - server health status
 * @returns {Error}  default - Unexpected error
 */
const health = (req, res) => {
  const healthResponse = healthLogic(version, build);
  res.status(healthResponse.status).json(healthResponse.data);
};

module.exports = { health };
