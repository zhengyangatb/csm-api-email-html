const router = require('express').Router();
const { health } = require('./health');
const { buildEmailHtml } = require('./buildEmailHtml');

const routes = (app) => {
  app.use('/', router.get('/', health));
  app.use('/health', router.get('/health', health));
  app.use('/buildEmailHtml', router.post('/buildEmailHtml', buildEmailHtml));
};

module.exports = { routes };
