const PROJECT_ID = process.env.PROJECT_ID || 'sbx-microservice-test';
const EMAIL_SUBSCRIPTION_NAME = process.env.EMAIL_SUBSCRIPTION_NAME || 'atb-notifications-email-sub';
const PUBSUB_KEY_PATH = process.env.GOOGLE_APPLICATION_CREDENTIALS || './pubsub-key.json';
const SENDGRID_USER_PASS_PATHNAME = process.env.SENDGRID_USER_PASS_PATHNAME || './sendgrid-user-pass.json';
const TOKENIZATION_URL = process.env.TOKENIZATION_URL || 'http://34.107.131.23/v1';

const config = {
  PROJECT_ID,
  EMAIL_SUBSCRIPTION_NAME,
  PUBSUB_KEY_PATH,
  SENDGRID_USER_PASS_PATHNAME,
  TOKENIZATION_URL,
  STATUS_ERROR: 400,
  STATUS_CREATED: 201,
  STATUS_OK: 200,
  STATUS_SMTP: 250,
};

module.exports = config;
