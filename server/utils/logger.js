/* eslint-disable */

const bunyan = require('bunyan');
const { Transform } = require('stream');

const level2severity = level => {
  const levelMap = {
    // fatal
    60: 'CRITICAL',
    // error
    50: 'ERROR',
    // warn
    40: 'WARNING',
    // info
    30: 'INFO',
    // debug
    20: 'DEBUG',
    // trace (Stackdriver has no equivalent level)
    10: 'DEBUG',
  };
  return levelMap[level] || 'DEFAULT';
};

class StackDriverStream extends Transform {
  constructor() {
    super({ writableObjectMode: true });
  }

  _transform(chunk, encoding, callback) {
    if (typeof chunk === 'string') {
      callback(new Error('Bunyan stream must be in "raw" mode'));
      return;
    }
    try {
      const entry = this.transformToStackDriver(chunk);
      callback(undefined, `${JSON.stringify(entry)}\n`);
    } catch (err) {
      callback(err);
    }
  }

  // eslint-disable-next-line class-methods-use-this
  transformToStackDriver(record) {
    const { msg, level, err, req, v, hostname, pid, ...rest } = record;
    const entry = {
      message: msg,
      severity: level2severity(level),
      ...rest,
    };

    // Support StackDriver Error Reporting
    if (err && err.stack) {
      if (record.userId) {
        // this needs to be `user` for stackdriver error affected users
        // https://google-cloud-python.readthedocs.io/en/0.32.0/error-reporting/client.html
        entry.context = { user: record.userId };
      }
      entry.message = err.stack;
      entry.serviceContext = { service: record.name };
    }

    if (req) {
      entry.httpRequest = req;
    }

    return entry;
  }
}

const defaultLogConfig = () => {
  const stream = new StackDriverStream();
  stream.pipe(process.stdout);

  return {
    name: process.env.POD_SVC || 'localhost',
    streams: [
      {
        level: 'debug',
        type: 'raw',
        stream,
      },
    ],
  };
};

const createLogger = (opts) => {
  const logopts = { ...defaultLogConfig(), ...opts };
  return bunyan.createLogger(logopts);
};

const logger = ({ type, message }) => {
  const loggerInstance = createLogger({});
  if (type === 'debug') {
    loggerInstance.debug(message);
  }
  if (type === 'error') {
    loggerInstance.error(message);
  }
};

module.exports = { createLogger, logger };
