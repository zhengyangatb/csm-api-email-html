const buildRequest = (method, url, additionalHeaders,
  data, params) => new Promise((resolve, reject) => {
  try {
    const headers = {
      'Content-Type': 'application/json',
      Accept: 'application/json',
      ...additionalHeaders,
    };
    const request = {
      url,
      method: method || 'GET',
      headers,
      params,
      data,
    };
    resolve(request);
  } catch (e) {
    const errorString = `Error occured while building request. Error: ${e}`;
    reject(new Error(errorString));
  }
});

module.exports = { buildRequest };
