FROM node:12-alpine

# Create app dir
WORKDIR /usr/src/app

# bundle app source
COPY . /usr/src/app

RUN npm install

EXPOSE 7892
CMD ["npm", "start"]
